#! /usr/bin/env python3
# -----------------------------------------------------------------------------
# how to run:
#    pytest-3 matrixmult.py
# -----------------------------------------------------------------------------
import pytest
import numpy as np
# -----------------------------------------------------------------------------
def get_size(A):
    # A - array
    try:
        second_dim = len(A[0])
    except:
        second_dim = 1
    return len(A), second_dim
def create(r,c):
    # Creates matrix with given size
    C=[]
    for i in range(r):
        C.append([0]*c)
    return C
def multiply(A, B):
    ar,ac = get_size(A)
    br,bc = get_size(B)
    if ac != br:
        raise ValueError('Wrong dimensions!')
    if ac == 1 and ar == 1:
        if br == 1 and bc == 1:
            return [[A[0][0]*B[0][0]]]
        else: # allow number-matrix multiplication
            for i in range(br):
                for j in range(bc):
                    C = create(br, bc)
                    C[i][j] = B[i][j] * A[0][0]
            #raise Warning('Multiplicating a number with an array')
            return C
    if bc == 1 and br == 1:
        for i in range(ar):
            for j in range(ac):
                C = create(ar,ac)
                C[i][j] = A[i][j] * B[0][0]
        #raise Warning('Multiplicating a number with an array')
        return C
    C = create(ar,bc)
    for i in range(ar):
        for j in range(bc):
            s=0
            for k in range(ac):
                s+= A[i][k]*B[k][j]
            C[i][j] = s
    return C
def multiplyWithNumpy(A, B):
    A = np.array(A)
    B = np.array(B)
    return np.dot(A,B)
# -----------------------------------------------------------------------------
testdata = [
    ([[3]], [[6]], [[18]]),
    ([[1, 2]],   [[3], [4]], [[11]]),
    ([[1, 2, 3], [4, 5, -7]],  [[3, 0, 4], [2, 5, 1], [-1, -1, 0]],  [[4, 7, 6], [29, 32, 21]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b,expected", testdata)
def test_multiply(multiplicationFunc, a, b, expected):
    assert np.array_equal(multiplicationFunc(a, b), expected)


testdataImpossibleProducts = [
    ([[3, 4]], [[6, 7]]),
    ([[1, 2]], [[3, 4], [4, 5], [5, 6]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b", testdataImpossibleProducts)
def test_multiplyIncompatibleMatrices(multiplicationFunc, a, b):
    with pytest.raises(Exception) as e:
        multiplicationFunc(a, b)



