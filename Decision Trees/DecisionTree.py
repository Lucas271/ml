#!/usr/bin/env
# -----------------------------------------------------------------------------
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.utils import shuffle
#from sklearn.model_selection import cross_val_score
#from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import tree

#import graphviz
import pydotplus
from sklearn.tree import export_graphviz

#import pydotplus
#from sklearn.externals.six import StringIO

from time import time
from datetime import datetime, timedelta
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def plot_tree(clf):
    iris = load_iris()
    dot_data = export_graphviz(clf, out_file=None,
                               feature_names=iris['feature_names'],
                                    class_names=iris['target_names'],
                                    filled=True,
                                    special_characters=True)
    #graph = graphviz.Source(dot_data)

    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.write_png("iris.png")
    return True
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def preprocess(p=0.8):
    # p - data devision proportion
    iris = load_iris()
    x = iris.data
    y = iris.target
    # Shuffle data
    x,y = shuffle(x,y)
    L = len(y)
    x_train = x[:int(p*L)]
    x_test = x[int(p*L):]
    y_train = y[:int(p*L)]
    y_test = y[int(p*L):]
    return x_train, y_train, x_test, y_test
# -----------------------------------------------------------------------------    
def get_tree(depth):
    x_train, y_train, x_test, y_test = preprocess()
    clf = tree.DecisionTreeClassifier(max_depth=depth,
                                      criterion = "gini")
    clf = clf.fit(x_train, y_train)
    
    y_pred = clf.predict(x_test)
    acc = accuracy_score(y_test, y_pred)
    return clf, acc
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if __name__ == "__main__":
# -----------------------------------------------------------------------------
    print('-'*20,'REPORT','-'*20)
    print(" "+str(datetime.now())[:-7])
    print("-"*20)
    print()
    start = time()
# -----------------------------------------------------------------------------
    MC = 1
    d_tab = [2,3,4,5,6,7,8,9,10]
    d_tab = [6]
    results = np.zeros((len(d_tab),MC))
    for mc in range(MC):
        for i in range(len(d_tab)):
            clf, acc = get_tree(d_tab[i])
            results[i,mc] = acc
    results = np.mean(results, axis=1)
    print(results)
    #plot_tree(clf)
# -----------------------------------------------------------------------------
    end = time()
    print('-'*48)
    print('Execution time: ',str(timedelta(seconds=end-start)))
    print('-'*48)
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
