#!/usr/bin/env
# -----------------------------------------------------------------------------
import numpy as np
import random
# -----------------------------------------------------------------------------
# Script based on CalTech talks and Andrew Ng's cource on ML
# -----------------------------------------------------------------------------
class Network(object):

    def __init__(self, sizes=[784, 30, 10], eta=0.1, mu=0, std=1):
        # sizes - list of numbers of nodes in each layer
        self.num_layers = len(sizes)
        self.sizes = sizes        
        # Leraning rate
        self.eta = eta
        self.mu = mu
        self.std = std
        #self.lambda = lambda
        # Weights
        self.weights = [std * np.random.randn(y, x) + mu
                        for x, y in zip(sizes[:-1], sizes[1:])]
        # Biases
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        return None

    def feedforward(self, x):
        #Return the output of the network
        layer = x
        for b, w in zip(self.biases, self.weights):
            layer = sigmoid(np.dot(w, layer)+b)
        return layer

    def SGD(self, training_data, mini_batch_size, eta=0):
        # Learning - Stochastic Gradient Descent
        # training_data   <--- (x,y)  [tuple]
        if eta == 0:
            eta = self.eta
        n = len(training_data)
        # Check if 'mini_batch_size' is correct
        while self.check_mini_batch_size(n, mini_batch_size):
            mini_batch_size +=1
        # Shuffle 'training_data'
        random.shuffle(training_data)
        # Deviding 'testing_data' into mini_batches
        mini_batches = [
            training_data[k:k+mini_batch_size]
            for k in range(0, n, mini_batch_size)]
        # Updating weights and biases basing on one mini_batch
        for mini_batch in mini_batches:
            self.update_mini_batch(mini_batch, eta)
        return None
    
    def update_mini_batch(self, mini_batch, eta):
        # mini_batch   <--- (x,y)  [tuple]
        #            x <--- input
        #            y <--- output (expected)
        # Weights nablas
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # Biases nablas
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        for x, y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]
        # weigts *= (1 - lambda)
        self.weights = [w-(eta/len(mini_batch))*nw
                        for w, nw in zip(self.weights, nabla_w)]
        self.biases = [b-(eta/len(mini_batch))*nb
                       for b, nb in zip(self.biases, nabla_b)]
        return None

    def backprop(self, x, y):
        # Weights nablas
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # Biases nablas
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        # Forward
        activation = x
        activations = [x] # list of the activations, layer by layer
        zs = [] # list of 'z' vectors              , layer by layer
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation)+b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
        # Backward
        delta = self.cost_derivative(activations[-1], y) * \
            sigmoid(zs[-1],deriv=True)
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())
        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid(z,deriv=True)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())
        return (nabla_b, nabla_w)

    def copy(self):
        # Returns a copy of the network
        new_net = Network(self.sizes, self.eta, self.mu, self.std)
        # Weights
        new_net.weights = [np.copy(w)
                           for w in self.weights]
        # Biases
        new_net.biases = [np.copy(b)
                          for b in self.biases]
        return new_net
	
    def cost_derivative(self, output, y):
        # Assumed Cost-function: 1/2 (output-y)^2
        return (output - y)
    
    def check_mini_batch_size(self, n, s):
        if n % s == 0:
            return True
        else:
            return False
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def sigmoid(z,deriv=False):
    if deriv:
        #Derivative of the sigmoid function.
        return sigmoid(z)*(1-sigmoid(z))
    else:
        #The sigmoid function.
        return 1.0/(1.0+np.exp(-z))
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
