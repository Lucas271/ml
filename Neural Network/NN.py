#!/usr/bin/env
# -----------------------------------------------------------------------------
# DEADLLINE: November 16
import numpy as np
from Network import Network
import mnist_loader

import matplotlib.pyplot as plt

from time import time
import datetime
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Plot the results
def draw(file_name, labels, parameter, folder="pics/", title=""):
    A = load_data(file_name)
    x = np.linspace(1,N_tab[0])
    for i in range(len(labels)):
        #plt.plot(N_tab, A[:,i], label=parameter+"={}".format(labels[i]))
        plt.plot(x, A[:,i], label=parameter+"={}".format(labels[i]))
        #plt.plot(x, A[:,i], "o", label=parameter+"={}".format(labels[i]))
    plt.title(title)
    plt.xlabel("Number of learning courses")
    plt.ylabel("accuracy")
    plt.legend()
    plt.savefig(folder+file_name.replace(".txt","")+'.png')
    plt.close()
    return None
# -----------------------------------------------------------------------------
# Read numpy-data from file_name
def load_data(file_name):
    return np.loadtxt(file_name)
# -----------------------------------------------------------------------------
def convert_output(vector): # in case of testing_data
    max_val = 0
    index = None
    for i in range(len(vector)):
        if vector[i] > max_val:
            max_val = vector[i]
            index=i
    return index
# -----------------------------------------------------------------------------
def get_score(net, testing_data):
    # Calculate SCORE
    cnt = 0
    for i in range(len(testing_data)):
        if convert_output( net.feedforward(testing_data[i][0]) ) == testing_data[i][1]:
            cnt+=1
    return cnt/len(testing_data)
# -----------------------------------------------------------------------------
# Generate ONLY ONE FILE
def Monte_Carlo(parameter, value, mu=0, std=1):
    # 1 - iterate over etas,           fix HiddenSize
    # 2 - iterate over hidden sizes,   fix Eta
    global eta_tab, hidden_size_tab
    if parameter == 1:
        hidden_size_tab = [value]
        #results = np.zeros((len(N_tab), len(eta_tab)))
        results = np.zeros((N_tab[0], len(eta_tab)))
        file_name = "M={}_HiddenSize={}_std={}.txt".format(M,hidden_size_tab[0],std)
    elif parameter == 2:
        eta_tab = [value]
        #results = np.zeros((len(N_tab), len(hidden_size_tab)))
        results = np.zeros((N_tab[0], len(hidden_size_tab)))
        file_name = "M={}_Eta={}_std={}.txt".format(M,eta_tab[0],std)
    for e in range(len(eta_tab)):
        for h in range(len(hidden_size_tab)):
            print("HiddenSize = {}, Eta = {}".format(hidden_size_tab[h], eta_tab[e]))
            #net_org = Network(sizes=[784, hidden_size_tab[h], 10],eta=eta_tab[e], mu=mu, std=std)
            # MONTE CARLO
            #result_m = np.zeros((len(N_tab),M))
            result_m = np.zeros((N_tab[0],M))
            for m in range(M):
                print("M={}".format(m+1))
                #net = net_org.copy()
                net = Network(sizes=[784, hidden_size_tab[h], 10],eta=eta_tab[e], mu=mu, std=std)
                #i = 0 # index
                #for n in range(1,N_tab[-1]+1):
                for n in range(N_tab[0]):
                    net.SGD(training_data, mini_batch_size=MB_size)
                    #print("|", sep=' ', end='', flush=True)
                    #if n in N_tab:
                    #    score = get_score(net, testing_data)
                    #    result_m[i,m] = score
                    #    i += 1 # index
                    score = get_score(net, testing_data)
                    result_m[n,m] = score
            if parameter == 1:
                results[:,e] = np.mean(result_m, axis=1)
            elif parameter == 2:
                results[:,h] = np.mean(result_m, axis=1)
    np.savetxt(file_name, results)
    #if std != 1:
    #    std = "1/sqrt(N)"
    #if parameter == 1:
    #    title = "M={}, HiddenSize={}, std={}".format(M,value,std)
    #    draw(file_name, eta_tab, "Eta", title=title)
    #elif parameter == 2:
    #    title = "M={}, Eta={}, std={}".format(M,value,std)
    #    draw(file_name, hidden_size_tab, "HiddenSize", title=title)
    return None
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
M = 10 # Monte Carlo simulations
#N_tab = [1, 3, 5, 8, 10, 12, 15, 17, 20, 30, 50]
N_tab = [50]
eta_tab = [0.01, 0.1, 0.5, 1, 3]
hidden_size_tab = [5, 10, 20, 30, 50]
MB_size = 10 # mini_batch_size
MB_size_tab = [10, 50, 100]

training_data, validation_data, testing_data = mnist_loader.load_data_wrapper()
training_data=list(training_data)
validation_data=list(validation_data)
testing_data=list(testing_data)


print('--------------------- Report --------------------')
print(str(datetime.datetime.now())[:-7])
print()
start = time()


std = 1/np.sqrt(N_tab[-1])


# Fix HiddenSize
#Monte_Carlo(1, 20)
#Monte_Carlo(1, 20, std=std)

# Fix Eta
#Monte_Carlo(2, 0.1)
#Monte_Carlo(2, 0.1, std=std)

#draw("12_11_2018_M=1_Eta=0.1_std=1.txt", hidden_size_tab, "HiddenSize")
#draw("12_11_2018_M=1_Eta=0.1_std=0.1414213562373095.txt", hidden_size_tab, "HiddenSize")
#draw("12_11_2018_M=1_HiddenSize=20_std=1.txt", eta_tab, "Eta")
#draw("12_11_2018_M=1_HiddenSize=20_std=0.1414213562373095.txt", eta_tab, "Eta")
    
    
end = time()
print('Execution time: ',str(datetime.timedelta(seconds=end-start)).split(".")[0])
print('-------------------------------------------------')
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# M = 10
# N_tab = [1, 3, 5, 8, 10, 12, 15, 20, 30, 50]
# hidden_size_tab = [5, 10, 20, 30, 50]
# Eta = 0.1
# std = 1
# Execution time:  8:00:20
# -----------------------------------------------------------------------------
# M = 10
# N_tab = [1, 3, 5, 8, 10, 12, 15, 20, 30, 50]
# eta_tab = [0.01, 0.1, 0.5, 1, 3, 5]
# HiddenSize = 20
# std = 1
# Execution time:  6:11:00 ??? < 6:30

# different Monte Carlo approach:
# Execution time:  6:09:00

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
