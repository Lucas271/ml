import numpy as np
#np.random.seed(7)

#from switch_backend import to_theano
#to_theano()
import os
os.system("set MKL_THREADING_LAYER=GNU")
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils, normalize
from keras.datasets import mnist
from keras import optimizers

from time import time
import datetime
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
def make_table(which, folder="data/"):
    today = str(datetime.datetime.now())[:19]\
    .replace("-","_").replace(" ","_-_").replace(":","_")
    if which == "Dense":
        file_name = today+"_Dense.csv"
        cols = ["h_num", "h_size", "dropout", "eta"]
    elif which == "Conv2D":
        file_name = today+"_Conv2D.csv"
        cols = ["h_num", "filters", "kernel", "dropout", "eta"]
    with open(folder+file_name,"wb") as f:
        f.write("epoch, batch_size, "+", ".join(cols)+\
                ", loss, score, exec_time")
        f.write("\n")
    return folder+file_name
# ------------------------------------------------------------------------------
def update_table(file_name, data):
    with open(file_name, "a") as f:
        f.write(", ".join(data))
        f.write("\n")
    return True
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# MODELS
# ------------------------------------------------------------------------------
def make_Dense(file_name, X_train, Y_train, X_test, Y_test,
               epochs, batch_size,
               h_num=1, h_size=128, lr=0.01, dr=0,
               optimizer="sgd"):
    # lr - learning rate
    # dr - dropout rate
    #MODEL
    model = Sequential()
    model.add(Flatten())
    model.add(Dense(h_size, activation='relu'))
    for h in range(h_num):
        model.add(Dense(h_size, activation='relu'))
        model.add(Dropout(dr))
    model.add(Dense(10, activation='softmax'))
    # COMPILE
    if optimizer == "sgd":
        optimizer = optimizers.SGD(lr=lr, nesterov=False)
    model.compile(optimizer=optimizer,
                  loss="sparse_categorical_crossentropy",
                  metrics=['accuracy'])
    # TRAINING
    start = time()
    for epoch in range(1,epochs+1):
        model.fit(X_train, Y_train,
              batch_size=batch_size,
              epochs=1,
              verbose=1)
        stop = time()
        loss, score = model.evaluate(X_test, Y_test, verbose=0)
        exec_time = stop-start
        data = [epoch, batch_size, h_num, h_size, dr, lr,
                loss, score, exec_time]
        update_table(file_name, list(map(str,data)))
    return True
# ------------------------------------------------------------------------------
def make_Conv2D(file_name, X_train, Y_train, X_test, Y_test,
                epochs, batch_size,
                filters=32, kernel_size=(3,3), h_num=1, dr=0.0,
                lr=0.01, optimizer="sgd"):
    # dr - dropout rate
    # lr - learning rate
    # MODEL
    model = Sequential()
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu',
                        input_shape=(1,28,28)))
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(dr))
    for h in range(h_num-1):
        model.add(Conv2D(filters=filters, kernel_size=kernel_size,
                        activation='relu'))
        model.add(Conv2D(filters=filters, kernel_size=kernel_size,
                        activation='relu'))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(dr))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))
    # COMPILE
    if optimizer == "sgd":
        optimizer = optimizers.SGD(lr=lr, nesterov=False)
    model.compile(optimizer=optimizer,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    #print model.get_config()
    # TRAINING
    start = time()
    for epoch in range(1,epochs+1):
        model.fit(X_train, Y_train,
              batch_size=batch_size,
              epochs=1,
              verbose=1)
        stop = time()
        loss, score = model.evaluate(X_test, Y_test, verbose=0)
        exec_time = stop-start
        data = [epoch, batch_size, h_num, filters, kernel_size, dr, lr,
                loss, score, exec_time]
        update_table(file_name, list(map(str,data)))
    return True
# ------------------------------------------------------------------------------
# Example from website
def get_model_example():
    model = Sequential()
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu',
                        input_shape=(1,28,28)))# (depth, width, height)
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2))) # reduce the num of parameters
    model.add(Dropout(0.25)) # prevent overfitting; remove 'bad' nodes
    model.add(Flatten())
    model.add(Dense(128, activation='relu')) # Fully connected layer
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax')) # Output layer
    model.compile(optimizer='adam',
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model
# ------------------------------------------------------------------------------
def test():
    model = Sequential()
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu',
                        input_shape=(1,28,28)))
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu'))
    model.add(Conv2D(filters=32, kernel_size=(3,3),
                        activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))
    optimizer = optimizers.SGD(lr=0.01, nesterov=False)
    model.compile(optimizer=optimizer,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
def run_Dense(X_train, Y_train, X_test, Y_test,
              epochs, batch_tab, h_nums, eta_tab, dr=0):
    file_name = make_table("Dense")
    # PREPROCESS
    X_train = normalize(X_train)
    X_test = normalize(X_test)
    # RUN MODELS
    h_size = 196 # 784
    for batch_size in batch_tab:
        for h_num in range(1,h_nums+1):
            for eta in eta_tab:
                make_Dense(file_name, X_train, Y_train, X_test, Y_test,
                   epochs, batch_size,
                   h_num, h_size, lr=eta, dr=dr)
    return True
# ------------------------------------------------------------------------------
def run_Conv2D(x_train, y_train, x_test, y_test,
              epochs, batch_tab, h_nums, eta_tab, dr=0):
    file_name = make_table("Conv2D")
    # PREPROCESS
    X_train = x_train.reshape(x_train.shape[0], 1, 28, 28).astype('float32')
    X_train /= 255
    X_test = x_test.reshape(x_test.shape[0], 1, 28, 28).astype('float32')
    X_test /= 255

    Y_train = np_utils.to_categorical(y_train, 10)
    Y_test = np_utils.to_categorical(y_test, 10)
    # RUN MODELS
    filters = 32
    kernel_size = (3,3)
    for batch_size in batch_tab:
        for h_num in range(1,h_nums+1):
            for lr in eta_tab:
                make_Conv2D(file_name, X_train, Y_train, X_test, Y_test,
                    epochs, batch_size, filters=filters, kernel_size=kernel_size,
                    h_num=h_num, dr=dr, lr=lr)
    return True
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# DATA
(X_train, y_train), (X_test, y_test) = mnist.load_data()



model_tab = ['sgd', 'adam', 'adadelta','nadam']



epochs = 10 # FIXED
batch_tab = [1, 5, 10, 20]
eta_tab = [0.005, 0.01, 0.02, 0.05, 0.1, 0.5]
h_nums = 5

dr = 0.0

# ------------------------------------------------------------------------------
#run_Dense(X_train, y_train, X_test, y_test,
#              epochs, batch_tab, h_nums, eta_tab, dr=dr)
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
epochs = 10 # FIXED
batch_tab = [1, 5, 10, 20]
eta_tab = [0.5]
h_nums = 2

dr = 0.25

run_Conv2D(X_train, y_train, X_test, y_test,
              epochs, batch_tab, h_nums=h_nums,
           eta_tab=eta_tab, dr=dr)
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
