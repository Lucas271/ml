import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

from time import time
from datetime import datetime, timedelta
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# PLOTTING
# ------------------------------------------------------------------------------
def plot_summary(folder = "data/"):
    file_name1 = "Dense.csv"
    file_name2 = "Conv2D.csv"
    df_1 = pd.read_csv(folder+file_name1, sep=", ", engine='python')
    df_2 = pd.read_csv(folder+file_name2, sep=", ", engine='python')


    df1 = df_1.copy()
    df2 = df_2.copy()

    # (Best) Dense
    df1 = df1.loc[df1['h_num']==2]
    df1 = df1.loc[df1['batch_size']==10]
    df1 = df1.loc[df1['eta']==0.05]
    df1 = df1.loc[df1['dropout']==0.1]
    x1 = list(df1['epoch'])
    y1 = list(df1['score'])
    z1 = list(df1['exec_time'])

    # (Best) Convolutional
    df2 = df2.loc[df2['h_num']==2]
    df2 = df2.loc[df2['batch_size']==10]
    df2 = df2.loc[df2['eta']==0.05]
    df2 = df2.loc[df2['dropout']==0]
    x2 = list(df2['epoch'])
    y2 = list(df2['score'])
    z2 = list(df2['exec_time'])


    fig = plt.figure(figsize=(10,6))

    ax = fig.add_subplot(211)
    ax.plot(x1,y1, label="Dense")
    ax.plot(x2,y2, label="Conv2D")
    ax.set_xlabel("epoch")
    ax.set_ylabel("accuracy")
    ax.set_title("Performance evolution")
    ax.legend()

    ax = fig.add_subplot(212)
    ax.plot(x1,z1, label="Dense")
    ax.plot(x2,z2, label="Conv2D")
    ax.set_xlabel("epoch")
    ax.set_ylabel("time [s]")


    #fig.suptitle("")
    fig.tight_layout()
    folder = "pics/"
    file_name = "summary_epoch_score_time"
    fig.savefig(folder+file_name+".png")
    plt.close()
    return True
# ------------------------------------------------------------------------------
def plot_many(df, xvar, yvar, var1, var2, fixed, subplot_size,
              folder="pics/"):
    # df - Data Frame
    # xvar - variable on X axis (string)
    # yvar - variable on Y axis (string)
    # var1 - variable - the same on each plot (string)
    # var2 - variable - different on each plot (string)
    # fixed - fixed variables (dictionary)
    # subplot_size = (i,j) - size of subplots (int, int)
    nrow = subplot_size[0]
    ncol = subplot_size[1]
    df1 = df.copy()
    
    file_name = [xvar, yvar]
    for fix in fixed.keys():
        df1 = df1.loc[df1[fix]==fixed[fix]]
        file_name.append(fix+"="+str(fixed[fix]).replace(".",""))
    file_name = "_".join(file_name)
    

    plt.figure(figsize=(10,6))
    #lines = []
    #labels = []
    vals = np.unique(df[var2].values)
    for i in range(len(vals)):
        title = var2+"="+str(vals[i])
        #print(100*nrow+10*ncol+i+1)
        plt.subplot(100*nrow+10*ncol+i+1)
        for v in np.unique(df[var1].values):
            df2 = df1.loc[df1[var1]==v]
            df2 = df2.loc[df2[var2]==vals[i]]
            #print(df2[[xvar,yvar,var1]])
            x = list(df2[xvar])
            y = list(df2[yvar])
            plt.plot(x,y,label=var1+"={}".format(v))
            #p = plt.plot(x,y)
            #lines.append(p)
            #labels.append(var1+"={}".format(v))
            if i == 1-1:
                plt.legend(loc="lower right", ncol=2)
            plt.xticks([1,4,7,10])
            plt.ylim(0.85,1)
            plt.xlabel(xvar)
            plt.ylabel(yvar)
            plt.title(title)
    #plt.figlegend(tuple(lines), tuple(labels), "upper right")
    plt.tight_layout()
    #plt.subplots_adjust(top=-1)
    plt.savefig(folder+file_name+".png")
    plt.close()
    return True
# ------------------------------------------------------------------------------
def get_best_one(df, var, fixed, head=1):
    # df - Data Frame
    # var - variable with biggest value to find (string)
    # fixed - fixed variables (dictionary)
    df1 = df.copy()
    for fix in fixed.keys():
        df1 = df1.loc[df1[fix]==fixed[fix]]
    #return df1.loc[df1[var].idxmax()]
    return df1.nlargest(head, var)
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------



# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------









file_name = "Dense.csv"
folder = "data/"
df = pd.read_csv(folder+file_name, sep=", ", engine='python')

drops = [0, 0.1, 0.25]
drops = [   0.1      ] # best one

for d in drops:
    plot_many(df, 'epoch', 'score', 'eta', 'batch_size',
         {'h_num':2,
          'dropout':d}, (2,2))
print("Dense")
print("-"*10)
print(get_best_one(df, 'score',
                   {'epoch':10}, head=10)[['batch_size', 'h_num',
                                           'dropout', 'eta', 'score',
                                           'exec_time']])  





file_name = "Conv2D.csv"
df = pd.read_csv(folder+file_name, sep=", ", engine='python')

plot_many(df, 'epoch', 'score', 'eta', 'batch_size',
          {'h_num':2,
           'dropout':0}, (2,2))

print("Conv2D")
print("-"*10)
print(get_best_one(df, 'score',
                   {'epoch':10}, head=10)[['batch_size', 'h_num',
                                           'dropout', 'eta', 'score',
                                           'exec_time']])





plot_summary()




#print("Total Exe Time: "+\
#      str(timedelta(seconds=sum(df.loc[df['epoch'] == 10]['exec_time'])))[:7])


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
