import json

def to_theano(file_name="keras.json", folder="C:/users/work/.keras/"):
	with open(folder+file_name,"r+") as f:
		data = json.load(f)
		data["backend"] = "theano"
		data["image_data_format"] = "channels_first"
		f.seek(0)
		#data["image_dim_ordering"] = "th"
		json.dump(data, f,indent=4)
		f.truncate()
	return None

def to_tensorflow(file_name="keras.json", folder="C:/users/work/.keras/"):
	with open(folder+file_name,"r+") as f:
		data = json.load(f)
		data["backend"] = "tf"
		data["image_data_format"] = "channels_last"
		f.seek(0)
		#data["image_dim_ordering"] = "th"
		json.dump(data, f,indent=4)
		f.truncate()
	return None